/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Ejercicio2;

/**
 *
 * @author Andre
 */
public class Hexadecimal {
    //metodo que convierte los numeros del 10-15 a A-F
    private String conversor(int decimal){
        switch (decimal) {
            case 10:
                return "A";
            case 11:
                return "B";
            case 12:
                return "C";
            case 13:
                return "D";
            case 14:
                return "E";
            case 15:
                return "F";
            default:
                throw new RuntimeException("El numero no se encuentra en el rango de 10-15");
        }
    }
    
    //Metodo Recursivo de Hexadecimal
    public String hexadecimalR(int decimal){
        if ((decimal / 16) == 0) return decimal + "";
        if ((decimal % 16) > 9) return hexadecimalR(decimal/16) + (conversor(decimal % 16));
        return hexadecimalR(decimal / 16) + (decimal % 16);
    }
    
    //Decorador del metodo recursivo
    public String hexadecimal(int decimal){
        if (decimal <= 0){
            throw new RuntimeException("El numero no se puede convertir");
        }
        return hexadecimalR(decimal);
    }
    
}
