/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Ejercicio1;

/**
 *
 * @author Andre
 */
public class TestNose {
    
    public static void main(String[] args) {
        NoSe n = new NoSe();
        
        System.out.println("Recursivo:");
        n.noSe(20, 2);
        System.out.println("--------------------------\nIterativo:");
        n.noSeIterativa(20, 2);
    }
    
}
