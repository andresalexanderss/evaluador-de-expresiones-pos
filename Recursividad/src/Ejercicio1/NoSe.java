/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Ejercicio1;

/**
 *
 * @author Andre
 */
public class NoSe {

    public void noSe(int numero, int factor) {
        if (numero > 1) {
            if ((numero % factor) == 0) {
                System.out.println(factor);
                noSe(numero / factor, factor);
            } else {
                noSe(numero, factor + 1);
            }
        }
    }

    public void noSeIterativa(int numero, int factor) {
        while (numero > 1) {
            if ((numero % factor) == 0) {
                System.out.println(factor);
                numero /= factor;
            } else {
                factor += 1;
            }
        }
    }

}
