/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Ejercicio3;

/**
 *
 * @author Andre
 */
public class CapicuaR {
    //Decorador del metodo Recursivo
    public boolean capicua(int decimal){
        if (decimal <= 0){
            throw new RuntimeException("El numero ingresado no es valido");
        }
        
        if (capicuaR(decimal).equals(decimal)) return true;
        
        return false;
    }
    
    //Metodo Recursivo
    public String capicuaR(int decimal){
        String cadenaInv = "";
        
        cadenaInv = capicuaR(decimal * -1);
        
        return cadenaInv;
    }
}
