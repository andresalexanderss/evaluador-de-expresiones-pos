package Modelo;

/**
 *
 * @author Andres
 */
public class NodoD<T> {
    
    private T info;
    private NodoD sig,ant;

    NodoD() {
    }

    NodoD(T info, NodoD sig, NodoD ant) {
        this.info = info;
        this.sig = sig;
        this.ant = ant;
    }

    T getInfo() {
        return info;
    }

    void setInfo(T info) {
        this.info = info;
    }

    NodoD getSig() {
        return sig;
    }

    void setSig(NodoD sig) {
        this.sig = sig;
    }

    NodoD getAnt() {
        return ant;
    }

    void setAnt(NodoD ant) {
        this.ant = ant;
    }
    
}