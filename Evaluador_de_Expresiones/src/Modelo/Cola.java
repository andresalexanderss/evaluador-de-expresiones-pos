/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;

/**
 *
 * @author Andre
 */
public class Cola<T> {
    
    private ListaCD <T> lista = new ListaCD();

    public Cola() {
        
    }
    
    public void enColar(T info){
        lista.addFin(info);
    }
    
    public T deColar (){
        if(this.lista.isVacia())
            throw new RuntimeException("La pila es vacia");
        return lista.remove(0);
    }
    
    public boolean esVacio(){
        return lista.isVacia();
    }
    
    public int getSize(){
        return lista.getSize();
    } 

}