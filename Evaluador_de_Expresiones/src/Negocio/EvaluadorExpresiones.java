/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Negocio;

import Modelo.ListaCD;
import Modelo.Pila;

/**
 *
 * @author Andre
 */
public class EvaluadorExpresiones {

    private String expresion_infija;

    public EvaluadorExpresiones() {
    }

    public EvaluadorExpresiones(String expresion_infija) {
        this.expresion_infija = expresion_infija;
    }

    public String getExpresion_infija() {
        return expresion_infija;
    }

    public void setExpresion_infija(String nuevaExpresion) {
        this.expresion_infija = nuevaExpresion;
    }

    public String getPrefijo() {
        ListaCD<String> expInf = evaluacionInfija();
        String[] proceso;
        String prefijo = "", dato = null, operadorPila = "", op1 = "", op2 = "", temp = "";
        Pila<String> operadores = new Pila();
        Pila<String> operandos = new Pila();

        for (int i = 0; i < expInf.getSize(); i++) {
            dato = expInf.get(i);
            if (isDigit(dato) || dato.equals("(")) {
                if (isDigit(dato))
                    operandos.push(dato);
                else operadores.push(dato);
            } else {
                if (esSigno(dato) || dato.equals(")")) {
                    if (!operadores.esVacio()) {
                        operadorPila = operadores.pop();
                        if (getPrioridadPila(operadorPila) >= getPrioridadExpre(dato)) {
                            op2 = operandos.pop();
                            op1 = operandos.pop();
                            temp = operadorPila + op1 + op2;
                            operandos.push(temp);
                            if (dato.equals(")")){
                                operadores.pop();
                            }
                        } else {
                            operadores.push(operadorPila);
                        }
                    }
                    if (!dato.equals(")")) {
                        operadores.push(dato);
                    }
                }
            }
        }

        while (!operadores.esVacio()) {
            operadorPila = operadores.pop();
            op2 = operandos.pop();
            op1 = operandos.pop();
            temp = operadorPila + op1 + op2;
            operandos.push(temp);
        }

        prefijo = operandos.pop();

        return prefijo;
    }

    public String getPostFijo() {
        ListaCD<String> expInf = evaluacionInfija();
        String posfijo = "", s = null;
        Pila<String> tokens = new Pila();
        String aux = "";

        for (int i = 0; i < expInf.getSize(); i++) {
            String dato = expInf.get(i);
            if (esSigno(dato)) {
                if (tokens.esVacio()) {
                    tokens.push(dato);
                } else {
                    int pe = getPrioridadExpre(dato);
                    int pp = getPrioridadPila(s = tokens.pop());
                    tokens.push(s);
                    if (pe > pp) {
                        tokens.push(dato);
                    } else {
                        if (dato.equals(")")) {
                            while (!(s = tokens.pop()).equals("(")) {
                                posfijo += "," + s;
                            }
                        } else {
                            posfijo += "," + tokens.pop();
                            tokens.push(dato);
                        }
                    } //sacar en un String tockens pila y volver a construirlo 
                }
            } else {
                posfijo += "," + dato;
            }
            aux += posfijo + "\n";
            
        }

        while (!tokens.esVacio()) {
            String n = tokens.pop();
            posfijo += "," + n;
            aux+=n;
        }

        return posfijo;
    }

    public float getEvaluadorExpresion(String tipo) {
        if (tipo.equals("Prefijo")){
            String[] tokensPre = this.getPrefijo().split("");
            Pila<Float> auxPre = new Pila();
            
            for(int i = tokensPre.length-1; i >= 0; i--){
                if(!esSigno(tokensPre[i])){
                    auxPre.push(Float.parseFloat(tokensPre[i]));
                } else {
                    if (auxPre.getSize() < 2){
                        throw new RuntimeException("La expresion en prefija esta mal escrita");
                    }
                    float num1 = auxPre.pop();
                    float num2 = auxPre.pop();
                    auxPre.push(this.getOperacion(tokensPre[i], num2, num1));
                }
            }
            
            return auxPre.pop();
        }
        if (tipo.equals("Postfijo")){
            String[] tokens = this.getPostFijo().split(",");
            Pila<Float> aux = new Pila();

            for (int i = 1; i < tokens.length; i++) {
                if (!this.esSigno(tokens[i])) {
                    aux.push(Float.parseFloat(tokens[i]));
                } else {
                    if (aux.getSize() < 2) {
                        throw new RuntimeException("La expresion es posfija esta mal escrita");
                    }
                    aux.push(this.getOperacion(tokens[i], aux.pop(), aux.pop()));
                }
            }

            return aux.pop();
        }
        
        return 0.0F;
    }

    private boolean esSigno(String c) {
        return c != null && (c.equals("+") || c.equals("-") || c.equals("*") || c.equals("/")
                || c.equals("^") || c.equals("(") || c.equals(")"));
    }

    private boolean isDigit(String c) {
        return c.matches("[0-9]+");
    }

    private int getPrioridadPila(String c) {
        if (c.equals("-") || c.equals("+")) {
            return 1;
        }
        if (c.equals("*") || c.equals("/")) {
            return 2;
        }
        if (c.equals("^")) {
            return 3;
        }
        if (c.equals("(")) {
            return 0;
        }
        return 0;
    }

    private int getPrioridadExpre(String c) {
        if (c.equals("^")) {
            return 4;
        }
        if (c.equals("*") || c.equals("/")) {
            return 2;
        }
        if (c.equals("-") || c.equals("+")) {
            return 1;
        }
        if (c.equals("(")) {
            return 5;
        }
        if (c.equals(")")){
            return -1;
        }
        return 0;
    }

    private float getOperacion(String op, float num2, float num1) {
        switch (op) {
            case "+":
                return num1 + num2;
            case "-":
                return num1 - num2;
            case "*":
                return num1 * num2;
            case "/":
                if (num2 == 0) {
                    throw new RuntimeException("No se puede efectuar la division por 0");
                }
                return num1 / num2;
            case "^":
                return (float) Math.pow(num1, num2);
        }
        return 0.0F;
    }

    private ListaCD<String> evaluacionInfija() {
        String aux = "";
        String auxAnt = "";
        boolean s = false;
        ListaCD<String> infijo = new ListaCD<>();
        String[] tokens = getExpresion_infija().split("");
        for (int i = 0; i < tokens.length; i++) {
            if (i == 0 && tokens[i].equals("-")) {
                aux += tokens[i];
            } else {
                if (isDigit(tokens[i])) {
                    aux += tokens[i];
                } else {
                    if (esSigno(tokens[i])) {
                        if (!aux.isEmpty()) {
                            infijo.addFin(aux);
                            aux = "";
                        }
                        if (tokens[i].equals("-")) {
                            if (esSigno(infijo.get(i - 1))) {
                                aux = "-";
                                s = true;
                            }
                        }
                        if (!s) {
                            infijo.addFin(tokens[i]);
                        }
                        s = false;
                    }
                }
            }
        }

        return infijo;
    }
}