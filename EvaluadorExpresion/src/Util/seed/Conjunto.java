/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Util.seed;

/**
 *  Wrapper (Envoltorio)
 * @author docente
 */
public class Conjunto<T> implements IConjunto<T>{
    
    private ListaS<T> elementos;

    public Conjunto() {
    }

    @Override
    public Conjunto<T> getUnion(Conjunto<T> c1) {
        
        return null;
    }

    @Override
    public Conjunto<T> getInterseccion(Conjunto<T> c1) {
        return null;
    }

    @Override
    public Conjunto<T> getDiferencia(Conjunto<T> c1) {
        return null;
    }

    @Override
    public Conjunto<T> getDiferenciaAsimetrica(Conjunto<T> c1) {
        return null;
       
    }

    @Override
    public T get(int i) {
        
        return null;
    }

    @Override
    public void set(int i, T info) {
        
    }

    @Override
    public void insertar(T info) {
        elementos.insertarInicio(info);
    }

    @Override
    public Conjunto<Conjunto<T>> getPotencia() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
}
