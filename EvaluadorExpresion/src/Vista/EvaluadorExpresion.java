/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package Vista;

import Util.seed.Pila;
import java.util.Scanner;

/**
 *
 * @author Andre
 */
public class EvaluadorExpresion {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String expresionPos = sc.nextLine();
        
        double resultado = evaluarPosfija(expresionPos);
        System.out.println("Resultado: " + resultado);
    }
    
    private static double evaluarPosfija(String expresion){
        String[] tokens = expresion.split(",");
        Pila<Double> result = new Pila();
        
        for (int i = 0; i < tokens.length; i++){
            if (!esSigno(tokens[i])){
                if (esNumero(tokens[i])){
                    result.push(Double.parseDouble(tokens[i]));
                }
            } else {
                if (result.size() < 2){
                    throw new RuntimeException("La expresión es posfija esta mal escrita");
                }
                result.push(getOperacion(tokens[i], result.pop(), result.pop()));
            }
        }
        
        return result.pop();
    }
    
    private static boolean esSigno(String c){
        return c != null && (c.equals("+") || c.equals("-") || c.equals("*") || c.equals("/") || c.equals("^"));
    }
    
    private static boolean esNumero(String c){
        return c.matches("[0-9]+");
    }
    
    private static double getOperacion(String op, double num2, double num1){
        switch (op) {
            case "+":
                return num1 + num2;
            case "-":
                return num1 - num2;
            case "*":
                return num1 * num2;
            case "/":
                if (num2 == 0){
                    throw new RuntimeException("No se puede dividir por 0");
                }
                return num1 / num2;
            case "^":
                return Math.pow(num1, num2);
        }
        return 0.0;
    }
}
